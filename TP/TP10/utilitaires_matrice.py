""" Fonctions utilitaires pour manipuler les matrices """

import API_matrice1 as matriceAPI

def get_diagonale_principale(matrice):
    """Renvoie la diagonale d'une matrice

    Args:
        ematrice (list): matrice sous forme ligne ligne ligne ou colonne, ligne, list

    Returns:
        list: Diagonale principale
    """    
    liste_diago = []
    for indice in range(matriceAPI.get_nb_lignes(matrice)):
        liste_diago.append(matriceAPI.get_val(matrice, indice, indice))
    return liste_diago

def get_diagonale_secondaire(matrice):
    """Renvoie la diagonale secondaire d'une matrice

    Args:
        matrice (list): matrice sous forme ligne ligne ligne ou colonne, ligne, liste

    Returns:
        list: Diagonale secondaire
    """   
    liste_diago = []
    for indice in range(matriceAPI.get_nb_lignes(matrice)):
        liste_diago.append(matriceAPI.get_val(matrice, indice,  matriceAPI.get_nb_lignes(matrice)-indice-1))
    return liste_diago

print(get_diagonale_secondaire([3, 3, [1, 2, 3, 4, 5, 6, 7, 8, 9]]))

def transpose(matrice):
    """Renvoie la transposee d'une matrice

    Args:
        matrice (list): matrice sous forme ligne ligne ligne ou colonne, ligne, list

    Returns:
        list: transpose
    """    
    matricev2 = matriceAPI.construit_matrice(matriceAPI.get_nb_colonnes(matrice), matriceAPI.get_nb_lignes(matrice), None)
    for indice_col in range(matriceAPI.get_nb_colonnes(matrice)):
        colonne_temporaire = matriceAPI.get_colonne(matrice, indice_col)
        for indice_ligne in range(len(colonne_temporaire)):
            matriceAPI.set_val(matricev2, indice_col, indice_ligne, colonne_temporaire[indice_ligne])
    return matricev2

print(transpose([3, 3, [1, 2, 3, 4, 5, 6, 7, 8, 9]]))