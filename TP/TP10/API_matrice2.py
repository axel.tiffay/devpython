import csv

def construit_matrice(nb_lignes, nb_colonnes, valeur_par_defaut=0):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    matrice = []
    indice = 0
    for _ in range(nb_lignes):
        matrice.append([])
        for _ in range(nb_colonnes):
            matrice[indice].append(valeur_par_defaut)
        indice += 1
    return matrice


def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return len(matrice)

def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return len(matrice[0])

def get_val(matrice, ligne, colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    return matrice[ligne][colonne]

def set_val(matrice, ligne, colonne, nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    matrice[ligne][colonne] = nouvelle_valeur
    return matrice

def get_ligne(matrice, ligne):
    """Renvoie la ligne d'une matrice

    Args:
        matrice (list): Matrice
        ligne (int): Numéro de la ligne souhaitée
    Returns:
        (list): liste de la ligne
    """    
    return matrice[ligne]

def get_colonne(matrice, colonne):
    """Renvoie la colonne d'une matrice

    Args:
        matrice (list): Matrice
        ligne (int): Numéro de la colonne souhaitée
    Returns:
        (list): liste de la colonne
    """    
    resultat = []
    for indice in range(len(matrice)):
        resultat.append(matrice[indice][colonne])
    return resultat