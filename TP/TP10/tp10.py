"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (name_poke, _) in pokedex:
        if name_poke == pokemon:
            return True
    return False


def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    type_atk = set()
    for (name_poke, attaque) in pokedex:
        if pokemon == name_poke:
            if attaque not in type_atk:
                type_atk.add(attaque)
    return type_atk


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    num = 0
    pokemon_set = set()
    for (name_poke, atk) in pokedex:
        if atk == attaque:
            if name_poke not in pokemon_set:
                pokemon_set.add(name_poke)
    return len(pokemon_set)



def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dic_freq = dict()
    for (_, attaque) in pokedex:
        if attaque not in dic_freq.keys():
            dic_freq[attaque] = 0
        dic_freq[attaque] += 1
    max = 0
    name = None
    for (key, number) in dic_freq.items():
        if number > max:
            max = number
            name = key
    return name


# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for name_poke in pokedex:
        if name_poke == pokemon:
            return True
    return False


def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    type_atk = set()
    for (name_poke, set_atk) in pokedex.items():
        if pokemon == name_poke:
            for attaque in set_atk:
                if attaque not in type_atk:
                    type_atk.add(attaque)
    return type_atk

def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    set_atk = set()
    for name_poke in pokedex:
        for atk in pokedex[name_poke]:
            if atk == attaque:
                if name_poke not in set_atk:
                    set_atk.add(name_poke)
    return len(set_atk)


def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dict_attaque = dict()
    for set_attaque in pokedex.values():
        for attaque in set_attaque:
            if attaque not in dict_attaque:
                dict_attaque[attaque] = 0
            dict_attaque[attaque] += 1
    max = 0
    name = None
    for (key, number) in dict_attaque.items():
        if number > max:
            max = number
            name = key
    return name
    

# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for set_poke in pokedex.values():
        if pokemon in set_poke:
            return True
    return False


def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    set_attaque = set()
    for (attaque, set_poke) in pokedex.items():
        if pokemon in set_poke:
            if attaque not in set_attaque:
                set_attaque.add(attaque)
    return set_attaque


def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    if attaque in pokedex:
        return len(pokedex[attaque])
    else:
        return 0


def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max = 0
    name_max = None
    for (attaque, set_poke) in pokedex.items():
        if len(set_poke) > max:
            max = len(set_poke)
            name_max = attaque
    return name_max
# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    dict_v2 = dict()
    for (pokemon, famille) in pokedex_v1:
        if pokemon not in dict_v2:
            dict_v2[pokemon] = set()
        dict_v2[pokemon].add(famille)
    return dict_v2


# Version 1 ==> Version 2

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    dict_v3 = dict()
    for (poke_name, set_attaque) in pokedex_v2.items():
        for attaque in set_attaque:
            if attaque not in dict_v3:
                dict_v3[attaque] = set()
            if poke_name not in dict_v3[attaque]:
                dict_v3[attaque].add(poke_name)
    return dict_v3

# =====================================================================
# Exercice 2 : Ecosystème
# =====================================================================

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    if animal in ecosysteme:
        if ecosysteme[animal] in ecosysteme or ecosysteme[animal] is None:
            return False
        else:
            return True
    return True



def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    animal_actuel = animal
    compteur = 0
    arret = False
    disparition = False
    while compteur <= len(ecosysteme) and not arret and not disparition:
        if extinction_immediate(ecosysteme, animal_actuel):
            disparition = True
        else:
            animal_actuel = ecosysteme[animal_actuel]
            if ecosysteme[animal_actuel] is None:
                arret = True
            else:
                compteur += 1
    return disparition

def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    ...


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    ...




#ex4 petite bete

def pokemons_par_famille(liste_pokemon):
    """Fonction qui renvoie un dictionnaire famille:nom_pokemon

    Args:
        liste_pokemon (list): Liste de tuples d'infos sur pokemon

    Returns:
        dict: [description]
    """    
    dict_finale = dict()
    for (pokemon, set_famille, _) in liste_pokemon:
        for famille in set_famille:
            if famille not in dict_finale:
                dict_finale[famille] = set()
            dict_finale[famille].add(pokemon)
    return dict_finale

print(pokemons_par_famille([
('Bulbizarre',{'Plante', 'Poison'}, '001. png'),
('Herbizarre', {'Plante', 'Poison'}, '002. png'),
('Abo', {'Poison'}, '023. png'),
('Jungko', {'Plante'}, '254. png')]))



