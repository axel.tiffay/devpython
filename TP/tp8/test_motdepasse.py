import motdepasse

# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_longueur_ok():
    assert motdepasse.longueur_ok("choubouilli") # longueur ok
    assert not motdepasse.longueur_ok("chou") # longueur pas ok
    assert not motdepasse.longueur_ok("") # chaine vide


def test_chiffre_ok():
    assert motdepasse.chiffre_ok("chou9bouilli", 1)  # chiffre au milieu
    assert motdepasse.chiffre_ok("7choubouilli", 1)  # chiffre au début
    assert motdepasse.chiffre_ok("choubouilli5", 1)  # chiffre à la fin
    assert motdepasse.chiffre_ok("chou3boui8lli", 1)  # deux chiffres    
    assert not motdepasse.chiffre_ok("chou", 1)       # pas de chiffres
    assert not motdepasse.chiffre_ok("un deux trois", 1) # pas de chiffres
    assert motdepasse.chiffre_ok("chou9bou77illi", 3) 
    assert motdepasse.chiffre_ok("7ch9oub8ouilli", 3)  
    assert motdepasse.chiffre_ok("chou4bou5illi5", 3)  



def test_sans_espace():
    assert motdepasse.sans_espace("choubouilli") # sans espace ok
    assert not motdepasse.sans_espace("chou bouilli") # espace au milieu
    assert not motdepasse.sans_espace(" choubouilli") # espace au début
    assert not motdepasse.sans_espace("choubouilli ") # espace à la fin
    assert motdepasse.sans_espace("") # chaine vide

def test_chiffre_min_repet():
    assert not motdepasse.chiffre_min_repet("oui2ahah2")
    assert not motdepasse.chiffre_min_repet("oui22ahah3")
    assert motdepasse.chiffre_min_repet("3ui2ahah3")


