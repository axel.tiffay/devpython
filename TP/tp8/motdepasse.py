# Codé par Papy Force X, jeune padawan de l'informatique

def dialogue_mot_de_passe():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        longeur = longueur_ok(mot_de_passe, 8)
        # je vérifie s'il y a un chiffre
        chiffre = chiffre_ok(mot_de_passe, 3)
        # je vérifie qu'il n'y a pas d'espace
        espace = sans_espace(mot_de_passe)
        # je vérifie si le plus petit chiffre ne se répéte pas
        chiffre_min_repete = chiffre_min_repet(mot_de_passe)
        # je vérifie si il n'y a pas de chiffre consecutif
        consec = chiffre_consecutif(mot_de_passe)
        # Je gère l'affichage
        mot_de_passe_correct = affichage_password(longeur, chiffre, espace, chiffre_min_repete, consec)
    ecrire_login_mdp_file(login, mot_de_passe, 'mdpUltraSecret.txt')
    return mot_de_passe

def longueur_ok(mot_de_passe, value):
    """Dis si le password fait au moins value de longueur

    Args:
        mot_de_passe (str): Mot de passe
        value (int): Valeur de longueur minimum

    Returns:
        bool: Vrai ou faux
    """    
    return len(mot_de_passe) >= value

def chiffre_ok(mdp, min):
    """Est-ce que le mot de passe contient assez de chiffre?

    Args:
        mdp (str): mot de passe
        min (int): nombre de chiffre minimal

    Returns:
        bool: Vrai ou faux
    """    
    lettre = 0
    while lettre != len(mdp):
        if mdp[lettre].isdigit():
            min -= 1
        if min == 0:
            return True
        lettre += 1
    return False

def chiffre_min_repet(mdp):
    """Est-ce que le chiffre le plus petit se répéte au moins une fois ?

    Args:
        mdp (str): Mot de passe

    Returns:
        bool: Vrai ou faux
    """    
    lettre = 0
    chiffre_petit = chiffre_plus_petit(mdp)
    chiffre_vu = False
    while lettre != len(mdp):
        if mdp[lettre].isdigit():
            if chiffre_petit == int(mdp[lettre]):
                if chiffre_vu:
                    return False
                chiffre_vu = True
        lettre += 1
    return True

def chiffre_plus_petit(mdp):
    """Quel est le chiffre le plus petit du mot de passe

    Args:
        mdp (str): Mot de passe

    Returns:
        int: Chiffre le plus petit
    """    
    lettre = 0
    chiffre_petit = 20
    while lettre != len(mdp):
        if mdp[lettre].isdigit():
            if chiffre_petit > int(mdp[lettre]):
                chiffre_petit = int(mdp[lettre])
        lettre += 1
    return chiffre_petit

chiffre_min_repet("3ui2ahah3")

def chiffre_consecutif(mdp):
    """Est-ce que le mot de passe contient des chiffres consécutifs ?

    Args:
        mdp (str): Mot de passe

    Returns:
        bool: Vrai ou faux
    """    
    lettre = 0
    while lettre != len(mdp):
        if mdp[lettre].isdigit():
            if consecutif:
                return True
            consecutif = True
        else:
            consecutif = False
        lettre += 1
    return False



def sans_espace(mdp):
    """Dis si le password contient un espace

    Args:
        mot_de_passe (str): Mot de passe

    Returns:
        bool: Vrai ou faux
    """    
    lettre = 0
    while lettre != len(mdp):
        if mdp[lettre] == " ":
            return False
        lettre += 1
    return True


def affichage_password(longeur, chiffre, espace, chiffre_min_repete, consec):
    """Gere l'affichage du mot de passe, si il a une erreur, renvoie False

    Args:
        longeur (bool): Est-il assez long ?
        chiffre (bool): Contient-il un chiffre ?
        espace (bool): Contient-il un espace ?

    Returns:
        bool: Mot de passe correct ?
    """    
    if not longeur:
        print("Votre mot de passe doit comporter au moins 8 caractères")
        return False
    elif not chiffre:
        print("Votre mot de passe doit comporter au moins trois chiffres")
        return False
    elif not espace:
        print("Votre mot de passe ne doit pas comporter d'espace")
        return False	
    elif not chiffre_min_repete:
        print("Le chiffre minimum à été répété au moins une fois !")
        return False   
    elif not consec:
        print("Il y a des chiffres consécutifs !")
        return False
    else:
        print("Votre mot de passe est correct")
        return True 

def ecrire_login_mdp_file(login, mdp, filename):
    """Créer un fichier filename avec le login et le mdp

    Args:
        login (str): Nom du login
        mdp (str): Nom du mot de passe
        filename (str): Nom du fichier
    """    
    fic = open(filename, 'w+') 
    fic.write(login)
    fic.write(mdp)
    fic.close()
    print("Fichier crée !")


#dialogue_mot_de_passe()

print('oui')