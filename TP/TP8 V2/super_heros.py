def intelligence_moyenne(dictionnaire):
    moyenne = 0
    for personnage in dictionnaire.values():
        moyenne += personnage[1]
    return moyenne/len(dictionnaire)
