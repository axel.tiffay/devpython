"""TP7 une application complète
ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""

import csv

LISTE_COMMUNES= [   ('18001', 'aaa', 1200),('18002', 'bbb', 71200),('18003', 'ccc', 520),
                    ('45001', 'ddd', 85200),('45002', 'abcd', 6350),('45003', 'aaa sur Loire', 8534),('45004', 'ggg', 5201)]

def afficher_menu(titre, liste_options):
    ligne = '+'
    for ind in range(len(titre)):
        ligne = ligne + '-'
    ligne = ligne + '--+'
    print(ligne)
    print('| ' + titre + ' |')
    print(ligne)
    for indice in range(len(liste_options)):
        print(str(indice+1) + ' -> ' + str(liste_options[indice]))

afficher_menu('NARUTO', ('akatsuki', 'bg', 'oui'))

def demander_nombre(message,borne_max):
    rep = input(message)
    if rep.isdecimal():
        rep = int(rep)
        if rep > 0 and rep <= borne_max:
            return rep
    return None


def menu(titre, liste_options):
    afficher_menu(titre,liste_options)
    res = demander_nombre('Entrez votre choix [' + str(1) + '-' + str(len(liste_options)) + '] ', 9)
    return res

def programme_principal():
    liste_options = ["Charger un fichier", "Rechercher la population d'une commune", "Afficher la population d'un département","Affiche la commune la plus peuplée d'un département","Afficher la liste des villes dans une tranche de population minimale/maximale.","Afficher la liste des top n villes","Population par département","Ecrire une commune dans un csv","Quitter"]
    liste_communes = []
    while True:
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi",liste_options[rep-1])
            nom_fic = input("Quel est le nom du fichier ? ")
            print("Il y a " + str(len(charger_fichier_population(nom_fic))) + " communes dans le csv.")
        elif rep == 2:
            print("Vous avez choisi",liste_options[rep-1])
            nom_fic = charger_fichier_population(input("Quel est le nom du fichier ? "))
            nom_ville = input("Quelle est le nom de la ville ? ")
            trouver = False
            for commune in nom_fic:
                if commune[1] == nom_ville:
                    print("La ville " + nom_ville + " a " + commune[2] + " habitants.")
                    trouver = True
            if not trouver:
                print("La ville n'est pas dans la liste")
        elif rep == 3:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep == 4:
            print("Vous avez choisi",liste_options[rep-1])
            print(commune_plus_peuplee_departement(input("Quelle est la liste de communes ? "), str(input("Quel est le département (2 chiffres) "))))
        elif rep == 5:
            print("Vous avez choisi",liste_options[rep-1])
            print(nombre_de_communes_tranche_pop(input("Quelle est la liste de communes ?  "), input("Population minimale ? "), input("Population maximale ? ")))
        elif rep == 6:
            print("Vous avez choisi",liste_options[rep-1])
            print(top_n_population(input("Quelle est la liste de communes ?  "), int(input("Nombre de villes dans le classement ? "))))
        elif rep == 7:
            print("Vous avez choisi",liste_options[rep-1])
            print(population_par_departement(input("Quelle est la liste de communes ? ")))
        elif rep == 8:
            print("Vous avez choisi",liste_options[rep-1])
            print(sauve_population_dpt(input("Quelle est le nom du fichier ? ")))
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")




def charger_fichier_population(nom_fic):
    """Charge un fichier csv en faisant une liste de tuples(depcom,com,ptot)

    Args:
        nom_fic (str): Nom du fichier csv

    Returns:
        list: Liste des tuples
    """
    fic = open(nom_fic, 'r')
    fic.readline()
    res = []
    for ville in fic:
        information = ville.split(";")
        res.append((information[0], information[1], information[2]))
    fic.close()
    return res

print(charger_fichier_population("population2017.csv"))

def liste_des_communes_commencant_par(liste_pop, debut_nom):
    """Crée une liste de communes commençant par debut_nom à partir de la liste_pop

    Args:
        liste_pop (list): Liste de communes
        debut_nom (str): Recherche du début du nom souhaitée

    Returns:
        list: Liste des villes commençant par debut_nom
    """
    liste_finale = []
    for commune in liste_pop:
        if commune[1].startswith(debut_nom):
            liste_finale.append(commune[1])
    return liste_finale


def commune_plus_peuplee_departement(liste_pop, num_dpt):
    """Cherche la ville la plus peuplée d'un departement

    Args:
        liste_pop (list): Liste de communes
        num_dpt (str): Numéro du département

    Returns:
        str: Nom de la ville la plus peuplée
    """    
    ville = (0,0,0)
    for commune in liste_pop:
        if commune[0].startswith(num_dpt):
            if ville[2] < commune[2]:
                ville = commune
    return ville[1]

def nombre_de_communes_tranche_pop(liste_pop, pop_min,pop_max):
    """Fonction qui retourne la liste des communes dans une tranche d'habitants

    Args:
        liste_pop (list): Liste de communes
        pop_min (int): Population minimale
        pop_max (int): Population maximale

    Returns:
        list: Toutes les communes dans la tranche
    """    
    liste_finale = []
    for commune in liste_pop:
        if commune[2] >= pop_min:
            if commune[2] <= pop_max:
                liste_finale.append(commune)
    return liste_finale

def ajouter_trier(commune, liste_pop, taille_max):
    """Ajoute une commune dans une liste de populations et le place au bon endroit (ordre décroissant)

    Args:
        commune (tuple): Commune
        liste_pop (list): Liste de communes
        taille_max (int): Pas utile

    Returns:
        list: Liste avec la commune ajoutée
    """    
    if len(liste_pop) + 1 <= taille_max:
        ind = 0
        placed = False
        while ind != len(liste_pop) and not placed:
            if int(liste_pop[ind][2]) < int(commune[2]):
                liste_pop.insert(ind, commune)
                placed = True
            ind += 1
        if not placed:
            liste_pop.append(commune)
    else:
        print("Liste dépasse liste_max")
    return liste_pop
                
    

def top_n_population(liste_pop, nb):
    """Affiche les nb populations les plus peuplées

    Args:
        liste_pop (list): Liste de communes
        nb (int): Nombre de commune que l'on veut afficher
    """    
    if nb > len(liste_pop):
        nb = len(liste_pop)
    if nb != 0:
        liste_tri = []
        for com in liste_pop:
            ajouter_trier(com, liste_tri, len(liste_pop))
        for indice in range(nb+1):
            print(indice+1, " --> ", liste_tri[indice][1], " avec ", 
            liste_tri[indice][2], " habitants .")
    else:
        print("Erreur")


def population_par_departement(liste_pop):
    """Renvoie une liste de couples département + habitants du dep

    Args:
        liste_pop (list): Liste de communes

    Returns:
        list: Liste des couples
    """    
    dep = int(liste_pop[0][0][:2])
    liste_res = []
    liste_res.append((dep,0))
    ind = 0
    deptype = 0
    while ind+1 != len(liste_pop):
        while liste_pop[ind][0][:2] == str(dep) and ind+1 != len(liste_pop):
            liste_res[deptype] = (liste_res[deptype][0],liste_pop[ind][2] + liste_res[deptype][1])
            ind += 1
        if ind+1 != len(liste_pop):
            dep = int(liste_pop[ind][0][:2])
            liste_res.append((dep,0))
            deptype += 1
    return liste_res

def sauve_population_dpt(nom_fic, liste_pop_dep):
    """Ajoute à un fichier csv une liste de communes

    Args:
        nom_fic (str): Nom du fichier csv
        liste_pop_dep ([type]): [description]
    """    
    fic = open(nom_fic, 'w')
    ecriture = csv.writer(fic)
    for elem in liste_pop_dep:
        ecriture.writerow(elem)
    fic.close()

# appel au programme principal
programme_principal()
