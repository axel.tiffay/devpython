import tp7_source

LISTE_COMMUNES= [   ('18001','aaa',1200),('18002','bbb',71200),('18003','ccc',520),
                    ('45001','ddd',85200),('45002','abcd',6350),('45003','aaa sur Loire',8534),('45004','ggg',5201)]

def test_liste_des_communes_commencant_par():
    assert tp7_source.liste_des_communes_commencant_par(LISTE_COMMUNES, "a") == ["aaa", "abcd", "aaa sur Loire"]
    assert tp7_source.liste_des_communes_commencant_par(LISTE_COMMUNES, "x") == []

test_liste_des_communes_commencant_par()

def test_commune_plus_peuplee_departement():
    assert tp7_source.commune_plus_peuplee_departement(LISTE_COMMUNES,"18") == "bbb"

test_commune_plus_peuplee_departement()

def test_nombre_de_communes_tranche_pop():
    assert tp7_source.nombre_de_communes_tranche_pop(LISTE_COMMUNES,1000,5300) == [('18001','aaa',1200),('45004','ggg',5201)]

test_nombre_de_communes_tranche_pop()

def test_ajouter_trier():
    assert tp7_source.ajouter_trier(('18001', 'aaa', 1200),[('18001', 'aaa', 1000),('18001', 'aaa', 800)], 5000) == [('18001', 'aaa', 1200),('18001', 'aaa', 1000),('18001', 'aaa', 800)]
    
test_ajouter_trier()


def test_top_n_population():
    assert tp7_source.top_n_population(LISTE_COMMUNES,2) == 2

def test_population_par_departement():
    ...
