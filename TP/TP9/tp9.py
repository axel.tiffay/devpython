"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    famille = set()
    for (_, pokemon) in pokedex:
        famille.add(pokemon)
    return famille

    #Complexité en O(N)


def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    number = 0
    for (_, pokemon) in pokedex:
        if pokemon == famille:
            number += 1
    return number

    #Complexité en O(N)

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    dico_frequence = dict()
    for (_, pokemon) in pokedex:
        if pokemon in dico_frequence:
            dico_frequence[pokemon] += 1
        else:
            dico_frequence[pokemon] = 1
    return dico_frequence

    #Complexité en O(N)

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_famille = dict()
    for (pokemon, famille) in pokedex:
        if famille not in dico_famille:
            dico_famille[famille] = set()
        dico_famille[famille].add(pokemon)
    return dico_famille

    #Complexité en O(N)

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    dict_famille = frequences_famille(pokedex)
    max_value = 0
    max_name = None
    for (famille, number) in dict_famille.items():
        if number > max_value:
            max_value = number
            max_name = famille
    return max_name
    #Complexité en O(N)



# ==========================
# La maison qui rend fou
# ==========================

def quel_guichet(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    for _ in range(len(mqrf)):
        if mqrf[guichet] == None:
            return guichet
        else:
            guichet = mqrf[guichet]
    return None



def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    for nb_guichets in range(len(mqrf)):
        if mqrf[guichet] == None:
            return (guichet,nb_guichets+1)
        else:
            guichet = mqrf[guichet]
    return None


def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    return quel_guichet_v2(mqrf, guichet)


# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    famille_dico = set()
    for famille_name in pokedex.values():
        for famille in famille_name:
            famille_dico.add(famille)
    return famille_dico

def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    number = 0
    for famille_pokedex in pokedex.values():
        trouver = False
        for fam in famille_pokedex:
            if fam == famille:
                if not trouver:
                    trouver = True
                    number += 1
    return number

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    dico_freq = dict()
    for pokemon in pokedex:
        for famille in pokedex[pokemon]:
            if famille not in dico_freq:
                dico_freq[famille] = 0
            dico_freq[famille] += 1
    return dico_freq


def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_famille = toutes_les_familles_v2(pokedex)
    dico_finale = dict()
    for pokemon in pokedex:
        for famille in pokedex[pokemon]:
            if famille not in dico_finale:
                dico_finale[famille] = set()
            dico_finale[famille].add(pokemon)
    return dico_finale

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    dico_freq = frequences_famille_v2(pokedex)
    nb_max = 0
    famille_max = None
    for (famille, values) in dico_freq.items():
        if int(values) > nb_max:
            nb_max = values
            famille_max = famille
    return famille_max
