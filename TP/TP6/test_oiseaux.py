import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,'Pie') == 'Corvidé'
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,'Pinson') == 'Passereau'
    assert oiseaux.recherche_oiseau(oiseaux.oiseaux,'Axel') is None

test_recherche_oiseau()

def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,'Corvidé') == ['Pie']
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,'Passereau') == ['Mésange', 'Moineau', 'Pinson', 'Rouge-gorge']
    assert oiseaux.recherche_par_famille(oiseaux.oiseaux,'Axel') is None

test_recherche_par_famille()

def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([]) == None

test_oiseau_le_plus_observe()

def test_est_liste_observations():
    assert not oiseaux.est_liste_observations(oiseaux.oiseaux)
    assert not oiseaux.est_liste_observations([('ok', 'ahah')])
    assert not oiseaux.est_liste_observations([('ok', 0)])
    assert oiseaux.est_liste_observations([('farm', 5),('zed', 2)])
    assert oiseaux.est_liste_observations([])
    assert not oiseaux.est_liste_observations([('ok', 5, 'hihihi')])

test_est_liste_observations()

def test_max_observations():
    assert oiseaux.max_observations(oiseaux.observations1) == 5
    assert oiseaux.max_observations(oiseaux.observations2) == 5
    assert oiseaux.max_observations(oiseaux.observations3) == 4
    assert oiseaux.max_observations([('fsdfsdf', 'fsdfs')]) == None

test_max_observations()

def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations1) == 3
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations2) == 15/6
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations3) == 15/6
    assert oiseaux.moyenne_oiseaux_observes([('fsdfsdf', 'fsdfs')]) == None

test_moyenne_oiseaux_observes()

def test_total_famille():
    assert oiseaux.total_famille('Passereau', oiseaux.oiseaux, oiseaux.observations1) == 8
    assert oiseaux.total_famille('Passereau', oiseaux.oiseaux, oiseaux.observations2) == 8
    assert oiseaux.total_famille('Passereau', oiseaux.oiseaux, oiseaux.observations3) == 7
    assert oiseaux.total_famille('Passereau', oiseaux.oiseaux, [('ahahah','ahahhah')]) == None

test_total_famille()

def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations([0,0,0,0,0],['chakra', 'chakra', 'chakra', 'chakra', 'chakra']) == []
    assert oiseaux.construire_liste_observations([1,0,3,0,7],['Subtile', 'chakra', 'celle-là', 'chakra', 'oui']) == [('Subtile', 1),('celle-là',3),('oui',7)]

test_construire_liste_observations()

d



