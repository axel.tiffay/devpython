# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4), ("Rouge-gorge",2)
            ]

# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if liste_observations != []:
        oiseau_max = liste_observations[0]
        for ind in range(len(liste_observations)):
            if liste_observations[ind][1] > oiseau_max[1]:
                oiseau_max = liste_observations[ind]
        return oiseau_max[0]
    return None
    
def recherche_oiseau(liste_famille,nom_oiseau):
    """Renvoie les caractéristiques du nom de l'oiseau entré

    Args:
        liste_famille (list): Registre des carac. des oiseaux
        nom_oiseau (str): Nom de l'oiseau cherché

    Returns:
        str: Nom des carac
    """    
    for reg in liste_famille:
        if reg[0] == nom_oiseau:
            return reg[1]
    return None

def recherche_par_famille(liste_famille,famille):
    """Renvoie tout les oiseaux appartenant à une famille donné

    Args:
        liste_famille (list): Registre des carac. des oiseaux
        famille (str): Nom de la famille donnée

    Returns:
        list: Liste de str d'oiseaux
    """
    resultat = []
    if liste_famille != []:
        for oiseaux in liste_famille:
            if oiseaux[1] == famille:
                resultat.append(oiseaux[0])
        if resultat == []:
            resultat = None
        return resultat
    return None

def est_liste_observations(liste_famille):
    """Regarde si une liste est une liste d'observations

    Args:
        liste_famille (list): Registre du nombre de specimens

    Returns:
        bool: Vrai si est une liste d'observation
    """
    prec = None
    for oiseaux in liste_famille:
        if len(oiseaux) != 2:
            return False
        if oiseaux[1] == 0:
            return False
        if prec == None:
            prec = oiseaux
        if prec > oiseaux:
            return False
        if not isinstance(oiseaux[0], str):
            return False
        if not isinstance(oiseaux[1], int):
            return False
    return True


def max_observations(liste_famille):
    """Donne le nombre d'observations le plus grand

    Args:
        liste_famille (list): Registre du nombre de specimens

    Returns:
        int: Le plus grand nombre d'observations
    """   
    res = (0,0)
    if est_liste_observations(liste_famille):
        for elem in liste_famille:
            if res[1] < elem[1]:
                res = elem
    else:
        return None
    return res[1]

def moyenne_oiseaux_observes(liste_famille):
    """Renvoie le nombre moyen de specimens observés pas espèces

    Args:
        liste_famille (list): Registre du nombre de specimens

    Returns:
        float: Moyenne des observations par espèces
    """    
    if est_liste_observations(liste_famille):
        somme = 0
        for oiseaux in liste_famille:
            somme += oiseaux[1]
        return somme / len(liste_famille)
    return None

def total_famille(famille, liste_famille, liste_observations):
    """Renvoie le nombre total de specimens observés dans une famille choisie

    Args:
        famille (str)): Nom de la famille
        liste_famille (list): Registre des carac. des oiseaux
        liste_observations (list): Registre du nombre de specimens

    Returns:
        int: Nombres de specimens observés
    """  
    if est_liste_observations(liste_observations):
        somme = 0
        oiseaux = recherche_par_famille(liste_famille, famille)
        for ind_fam in range(len(oiseaux)):
            for ind_obs in range(len(liste_observations)):
                if oiseaux[ind_fam] == liste_observations[ind_obs][0]:
                    somme += liste_observations[ind_obs][1]
        return somme
    return None

def construire_liste_observations(comptage, famille):
    """Fonction qui crée une liste d'observations avec une liste de compatage et une liste de famille.

    Args:
        comptage (list): Liste d'entiers
        famille (list): Liste de familles

    Returns:
        list: Liste d'observations tuple famille, comptage
    """ 
    liste_obs = []
    if len(comptage) == len(famille):
        for indice in range(len(comptage)):
            if comptage[indice] != 0:
                liste_obs.append((famille[indice],comptage[indice]))
    return liste_obs

def ask_specimens_liste_obs(liste_famille):
    """Fonction qui crée une liste d'observation en fonction des réponses de l'utilisateur en posant une question par famille

    Args:
        liste_famille (list): Liste de familles

    Returns:
        list: Liste d'observations tuple famille, comptage
    """
    liste_obs = []
    for oiseaux in liste_famille:
        res = input("Combien de " + oiseaux + " avez-vous vu ? ")
        if res != 0:
            liste_obs.append((oiseaux,res))
    return liste_obs

print(ask_specimens_liste_obs(['oui','naruto','ok','sakura']))
#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)