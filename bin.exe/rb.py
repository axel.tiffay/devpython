def range_betement(ldc):
    liste_malle = [[]]
    compteur = 0
    for indice in range(len(ldc)):
        compteur += ldc[indice][1]
        if compteur <= 50:
            liste_malle[len(liste_malle)-1].append(ldc[indice])
        else:
            liste_malle.append([ldc[indice]])
            compteur = ldc[indice][1]
    return liste_malle



liste_cado = [('Stylo', 25), ('Bonbon', 22), ('Sucette', 2), ('Coca', 50), ('IceTea', 22)]
assert range_betement(liste_cado) == [[('Stylo', 25), ('Bonbon', 22), ('Sucette', 2)], [('Coca', 50)], [('IceTea', 22)]]

def range_bien(ldc):
    ldc = sorted(ldc, reverse=True, key = lambda x: x[1])
    traineau = []
    while ldc != []:
        compteur = 0
        malle = []
        indice_to_pop = []
        for indice in range(len(ldc)):
            cadeau = ldc[indice]
            if compteur + cadeau[1] <= 50:
                compteur += cadeau[1]
                malle.append(cadeau)
                indice_to_pop.append(indice)
        traineau.append(malle)
        decalage = 0
        for indice in indice_to_pop:
            if ldc != []:
                ldc.pop(indice-decalage)
                decalage += 1
    return traineau

print(range_bien(liste_cado))

            