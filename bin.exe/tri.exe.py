def max_suite(liste):
    liste_finale = []
    for nombre in liste:
        if liste_finale == []:
            liste_finale.append([nombre])
        else:
            stop = False
            indice = 0
            while not stop and indice != len(liste_finale):
                if nombre + 1 == liste_finale[indice][-1]:
                    if nombre not in liste[indice]:
                        liste_finale[indice].append(nombre)
                    stop = True
                if nombre + -1 == liste_finale[indice][-1]:
                    if nombre not in liste[indice]:
                        liste_finale[indice].insert(0,nombre)
                    stop = True
                if nombre in liste_finale[indice]:
                    stop = True
                indice += 1
            if not stop:
                liste_finale.append([nombre])
    return liste_finale

#print(max_suite([19,11,5,19,15,13,11,6,21,10,19,6,12,17,20]))




def la_vraie_max_suite(liste):
    liste = sorted(liste)
    suite = [liste[0]]
    max_suite = []
    for nombre in liste:
        if suite[len(suite)-1] + 1 == nombre:
            suite.append(nombre)
        if nombre not in suite:
            if len(max_suite) < len(suite):
                max_suite = suite
            suite = [nombre]
    return max_suite

print(la_vraie_max_suite([19,11,5,19,15,13,11,6,21,10,19,6,12,17,20]))



def ajoute(nombre_add, liste):
    insert = False
    indice = 0
    while not insert and indice != len(liste):
        if nombre_add <= liste[indice] :
            liste.insert(indice, nombre_add)
            insert = True
        indice += 1
    if not insert:
        liste.append(nombre_add)
    return liste

print(ajoute(14,sorted([19,11,5,19,15,13,11,6,21,10,19,6,12,17,20])))
        